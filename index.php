<?php

/*Отладка*/
/*error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);*/

/*Старт приложения*/
require_once __DIR__  . '/vendor/autoload.php';

use Tracy\Debugger;
Debugger::enable();

use \Curl\Curl;

class FootbalParser {
	public $codes = ['200' => 0, '404' => 0, '503' => 0];

	public $curl;
	public $saw;

	/*const START_ID = 57770;
	const END_ID = 57780;*/

	const REPEAT = 5; // count repeat if 503;

	public $skiped = [];

	public $param = ['index.php', 1, 1000];

	public function __construct($param) {
		$this->param = $param;
		ORM::configure('mysql:host=localhost;dbname=football');
		ORM::configure('username', 'root');
		ORM::configure('password', '');
		ORM::configure('driver_options', [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);

		$this->curl = new Curl();
		$this->curl->setopt(CURLOPT_TIMEOUT, 3);
	}

	private function parseHTML($html) {
		$this->saw = new nokogiri($html);
		$player = [];
		$player['player']['name'] = trim($this->saw->get('table.imageHead tr td')->toArray()[1]['h1'][0]['#text'][0]);
		$player['player']['role'] = trim($this->saw->get('.midfielder')->toArray()[0]['#text'][0]);
		$player['player']['team'] = trim($this->saw->get('.midfielder')->toArray()[0]['a'][0]['#text'][0]);

		//Left block
		$player['player']['real_name'] =     trim($this->saw->get('.clubInfo')->toArray()[0]['tr'][0]['td'][0]['strong'][0]['#text'][0]);
		$player['player']['age'] =           trim($this->saw->get('.clubInfo')->toArray()[0]['tr'][1]['td'][0]['strong'][0]['#text'][0]);
		$player['player']['date_of_birth'] = trim($this->saw->get('.clubInfo')->toArray()[0]['tr'][1]['td'][0]['#text'][1]);
		$player['player']['height'] =        trim($this->saw->get('.clubInfo')->toArray()[0]['tr'][2]['td'][0]['strong'][0]['#text'][0]);
		$player['player']['weight'] =        trim($this->saw->get('.clubInfo')->toArray()[0]['tr'][3]['td'][0]['strong'][0]['#text'][0]);

		//Right block
		$player['player']['place_of_birth'] = trim($this->saw->get('.clubInfo')->toArray()[1]['tr'][0]['td'][0]['strong'][0]['#text'][0]);
		$player['player']['nationality'] =    trim($this->saw->get('.clubInfo')->toArray()[1]['tr'][1]['td'][0]['strong'][0]['#text'][0]);
		$player['player']['date_signed'] =    trim($this->saw->get('.clubInfo')->toArray()[1]['tr'][2]['td'][0]['strong'][0]['#text'][0]);
		$player['player']['fee'] =            trim($this->saw->get('.clubInfo')->toArray()[1]['tr'][3]['td'][0]['strong'][0]['#text'][0]);

		$rows = $this->saw->get('table.career tbody tr')->toArray();
		foreach ($rows as $key => $value) {
			if ($value['class'] == 'total') {
				continue;
			}
			$player['career'][]['club']        = trim($value['td'][0]['a'][0]['#text'][0]);
			$player['career'][]['from_date']   = trim($value['td'][1]['#text'][0]);
			$player['career'][]['to_date']     = trim($value['td'][2]['#text'][0]);
			$player['career'][]['fee']         = trim($value['td'][3]['#text'][0]);
			$player['career'][]['league_apps'] = trim($value['td'][4]['#text'][0]);
			$player['career'][]['league_gls']  = trim($value['td'][5]['#text'][0]);
			$player['career'][]['fa_cup_apps'] = trim($value['td'][6]['#text'][0]);
			$player['career'][]['fa_cup_gls']  = trim($value['td'][7]['#text'][0]);
			$player['career'][]['other_apps']  = trim($value['td'][8]['#text'][0]);
			$player['career'][]['other_gls']   = trim($value['td'][9]['#text'][0]);
		}

		return $player;	
	}

	private function getHTML($id) {
		return $this->curl->get("http://www.soccerbase.com/players/player.sd?player_id={$id}");
	}

	public function lets_parse() {
		echo "\tParse START. START_ID = " . $this->param[1] . " END_ID = " . $this->param[2] . "\n\n";

		$start = microtime(true);
		$repeat = 0;

		for ($i = $this->param[1]; $i <= $this->param[2]; $i++) {
			$strt = microtime(true);
			$html = $this->getHTML($i);

			switch ($this->curl->getinfo(CURLINFO_HTTP_CODE)) {
				case 200:
					$player = $this->parseHTML($html);
					if ($player['player']['name']) {
						$this->codes['200'] += 1;
						$person = ORM::for_table('players')->create($player['player']);
						$person->save();
						
						if (isset($player['career'])) {
							foreach ($player['career'] as $key => $value) {
								$value['player_id'] = $person->id();
								ORM::for_table('career')->create($value)->save();
							}
						}

						echo "\tID: {$i}  Player: {$player['player']['name']}  Time: ".(microtime(true) - $strt). "\n";
					} else {
						$this->codes['404'] += 1;
						array_push($this->skiped, $i);
						echo 'Error: 404 : ' . $this->curl->errorMessage . " Time: " . (microtime(true) - $strt) . "\n";
						continue 2;
					}
					break;

				case 404:
					$this->codes['404'] += 1;
					array_push($this->skiped, $i);
					echo 'Error: 404 : ' . $this->curl->errorMessage . " Time: " . (microtime(true) - $strt) . "\n";
					continue 2;
					break;

				case 0:
				case 28:
				case 503:
					echo 'Error: 503 : ' . $this->curl->errorMessage . " Time: " . (microtime(true) - $strt) . "\n";
					
					$repeat++;
					if ($repeat <= self::REPEAT) {
						$i--;
						echo "\tTry again {$repeat} of " . self::REPEAT . "\n";
						continue 2;
					} else {
						$this->codes['503'] += 1;
						array_push($this->skiped, $i);
						echo "\tSorry :( Can't get HTML\n";
					}
					break;			

				default:
					array_push($this->sciped, $i);
					$code = $this->curl->getinfo(CURLINFO_HTTP_CODE);
					$codes["{$code}"] = isset($codes["{$code}"]) ? $codes["{$code}"] + 1 : 1;
					echo 'Error: ' . $code . ' : ' . $this->curl->errorMessage . " Time: " . (microtime(true) - $strt) . "\n";
					break;
			}
			$repeat = 0;
		}

		echo "\nParse DONE. Time: ".(microtime(true) - $start) . "\n";
		$count = $this->param[2] - $this->param[1] + 1;
		echo "Success: {$this->codes['200']} of {$count}\n";
		echo "Response codes:\n";		
		foreach ($this->codes as $code => $value) {
			echo "\t" . $code . ': ' . $value . "\n";
		}

		if ($this->skiped) {
			echo "\nSkiped ID: " . implode(', ', $this->skiped) . "\n";
		}

	}
}
if ($argc < 3) {
	echo 'Need 2 params: start_id, end_id';
	die;
}

if ($argv[2] < $argv[1]) {
	echo 'Error: param end_id < start_id';
	die;
}

$football = new FootbalParser($argv);
$football->lets_parse();